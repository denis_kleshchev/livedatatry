package com.dkleshchev.livedatatest.mvp

import android.arch.lifecycle.Lifecycle
import android.os.Bundle

interface BaseContract {

    interface View

    interface Presenter<V : View> {

        fun attachLifecycle(lifecycle: Lifecycle)

        fun detachLifecycle(lifecycle: Lifecycle)

        fun attachView(view: V)

        fun detachView()

        fun getView(): V?

        fun isViewAttached(): Boolean

        fun onPresenterCreated()

        fun onPresenterDestroy()

        fun getStateBundle(): Bundle
    }
}