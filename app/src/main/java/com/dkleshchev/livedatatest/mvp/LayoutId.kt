package com.dkleshchev.livedatatest.mvp

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class LayoutId(val layout: Int = DefaultLayoutId.LAYOUT_NOT_DEFINED)

object DefaultLayoutId {
    const val LAYOUT_NOT_DEFINED = -1
}