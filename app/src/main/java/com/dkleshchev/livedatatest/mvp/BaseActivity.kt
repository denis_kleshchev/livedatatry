package com.dkleshchev.livedatatest.mvp

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity<V : BaseContract.View, P : BaseContract.Presenter<V>> :
        AppCompatActivity(), BaseContract.View {

    protected lateinit var presenter: P

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val viewModel = ViewModelProviders.of(this).get(BaseViewModel::class.java) as BaseViewModel<V, P>

        if (viewModel.presenter == null) {
            viewModel.presenter = initPresenter()
        }

        presenter = viewModel.presenter!!
        presenter.attachLifecycle(lifecycle)
        presenter.attachView(this as V)

        setContentView()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachLifecycle(lifecycle)
        presenter.detachView()
    }

    protected abstract fun initPresenter(): P

    private fun setContentView() {
        val layoutId = javaClass.getAnnotation(LayoutId::class.java).layout
        if (layoutId != DefaultLayoutId.LAYOUT_NOT_DEFINED) {
            setContentView(layoutId)
        }
    }
}