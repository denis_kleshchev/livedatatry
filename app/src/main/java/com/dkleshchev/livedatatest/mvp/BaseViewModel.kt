package com.dkleshchev.livedatatest.mvp

import android.arch.lifecycle.ViewModel

class BaseViewModel<V : BaseContract.View, P : BaseContract.Presenter<V>> : ViewModel() {

    var presenter: P? = null
        set(value) {
            if (field == null) {
                field = value
            }
        }

    override fun onCleared() {
        super.onCleared()
        presenter?.onPresenterDestroy()
        presenter = null
    }
}