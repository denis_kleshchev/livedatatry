package com.dkleshchev.livedatatest.mvp

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.os.Bundle
import android.support.annotation.CallSuper

abstract class BasePresenter<V : BaseContract.View> : LifecycleObserver, BaseContract.Presenter<V> {

    private var viewInstance: V? = null
    private var stateBundle = Bundle()

    override fun attachLifecycle(lifecycle: Lifecycle) =
            lifecycle.addObserver(this)

    override fun detachLifecycle(lifecycle: Lifecycle) =
            lifecycle.removeObserver(this)

    override fun getView(): V? = viewInstance

    override fun attachView(view: V) {
        viewInstance = view
    }

    override fun detachView() {
        viewInstance = null
    }

    override fun isViewAttached(): Boolean = viewInstance != null

    override fun getStateBundle(): Bundle = stateBundle

    @CallSuper
    override fun onPresenterCreated() {
        //stub
    }

    @CallSuper
    override fun onPresenterDestroy() {
        if (stateBundle.isEmpty.not()) {
            stateBundle.clear()
        }
    }
}