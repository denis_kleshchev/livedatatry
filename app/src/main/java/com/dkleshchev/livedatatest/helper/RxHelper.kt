package com.dkleshchev.livedatatest.helper

import android.os.AsyncTask
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class RxHelper : CompositeHelper {

    val completeable by completableDelegate()
    val flowable by flowableDelegate()
    val maybe by maybeDelegate()
    val observable by observableDelegate()
    val single by singleDelegate()
    val subject by subjectDelegate()

    private val composite: CompositeDisposable = CompositeDisposable()
    private val map: MutableMap<String, Disposable> = HashMap()

    val asyncScheduler: Scheduler = Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR)
    val mainScheduler: Scheduler = AndroidSchedulers.mainThread()

    override fun add(disposable: Disposable): Disposable = disposable.apply {
        composite.add(this)
    }

    override fun add(tag: String, disposable: Disposable): Disposable = disposable.apply {
        composite.add(this)

        if (map.containsKey(tag)) {
            composite.remove(map.getValue(tag))
        }
        map.put(tag, disposable)
    }

    override fun remove(disposable: Disposable): Disposable = disposable.apply {
        composite.remove(disposable)
    }

    override fun remove(tag: String): Disposable? = if (map.containsKey(tag)) {
        map.getValue(tag).apply {
            remove(this)
            map.remove(tag)
        }
    } else {
        null
    }

    override fun clear() {
        composite.clear()
        map.clear()
    }

    override fun inProgress(disposable: Disposable?): Boolean = disposable?.isDisposed?.not() ?: false

    override fun inProgress(tag: String): Boolean = inProgress(map[tag])
}