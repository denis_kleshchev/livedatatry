package com.dkleshchev.livedatatest.helper

import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.subjects.BehaviorSubject

class ObservableHelper internal constructor(private val helper: CompositeHelper,
                                            private val asyncScheduler: Scheduler,
                                            private val mainScheduler: Scheduler) {

    fun <T> applySchedulers(): ObservableTransformer<T, T> =
            applySchedulers(asyncScheduler, mainScheduler)

    fun <T> applySchedulers(subscribeOn: Scheduler,
                            observeOn: Scheduler): ObservableTransformer<T, T> =
            ObservableTransformer { upstream -> return@ObservableTransformer upstream.subscribeOn(subscribeOn).observeOn(observeOn) }

    fun <T> subscribe(observable: Observable<T>): Disposable = helper.add(observable.subscribe())

    fun <T> subscribe(observable: Observable<T>, next: (T) -> Unit): Disposable =
            helper.add(observable.subscribe(next))

    fun <T> subscribe(observable: Observable<T>, next: (T) -> Unit, error: (Throwable) -> Unit): Disposable =
            helper.add(observable.subscribe(next, error))

    fun <T> subscribe(observable: Observable<T>, next: (T) -> Unit, error: (Throwable) -> Unit, complete: () -> Unit): Disposable =
            helper.add(observable.subscribe(next, error, complete))

    fun <T> subscribe(observable: Observable<T>, observer: DisposableObserver<T>): Disposable =
            helper.add(observable.subscribeWith(observer))

    fun <T> bindPairSubject(observable: Observable<T>, subject: BehaviorSubject<T>): Disposable =
            subscribe(observable, { subject.onNext(it) }, { subject.onError(it) })

    fun <T> subscribe(tag: String, observable: Observable<T>): Disposable =
            helper.add(tag, observable.subscribe())

    fun <T> subscribe(tag: String, observable: Observable<T>, next: (T) -> Unit): Disposable =
            helper.add(tag, observable.subscribe(next))

    fun <T> subscribe(tag: String, observable: Observable<T>, next: (T) -> Unit, error: (Throwable) -> Unit): Disposable =
            helper.add(tag, observable.subscribe(next, error))

    fun <T> subscribe(tag: String, observable: Observable<T>, next: (T) -> Unit, error: (Throwable) -> Unit, complete: () -> Unit): Disposable =
            helper.add(tag, observable.subscribe(next, error, complete))

    fun <T> subscribe(tag: String, observable: Observable<T>, observer: DisposableObserver<T>): Disposable =
            helper.add(tag, observable.subscribeWith(observer))
}

fun RxHelper.observableDelegate(): Lazy<ObservableHelper> = lazy {
    ObservableHelper(this, asyncScheduler, mainScheduler)
}