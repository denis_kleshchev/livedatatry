package com.dkleshchev.livedatatest.helper

import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.SingleTransformer
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.subjects.BehaviorSubject

class SingleHelper internal constructor(private val helper: CompositeHelper,
                                        private val asyncScheduler: Scheduler,
                                        private val mainScheduler: Scheduler) {

    fun <T> applySchedulers(): SingleTransformer<T, T> =
            applySchedulers(asyncScheduler, mainScheduler)

    fun <T> applySchedulers(subscribeOn: Scheduler,
                            observeOn: Scheduler): SingleTransformer<T, T> =
            SingleTransformer { upstream -> return@SingleTransformer upstream.subscribeOn(subscribeOn).observeOn(observeOn) }

    fun <T> subscribe(single: Single<T>): Disposable = helper.add(single.subscribe())

    fun <T> subscribe(single: Single<T>, next: (T) -> Unit): Disposable =
            helper.add(single.subscribe(next))

    fun <T> subscribe(single: Single<T>, next: (T) -> Unit, error: (Throwable) -> Unit): Disposable =
            helper.add(single.subscribe(next, error))

    fun <T> subscribe(single: Single<T>, observer: DisposableSingleObserver<T>): Disposable =
            helper.add(single.subscribeWith(observer))

    fun <T> bindPairSubject(single: Single<T>, subject: BehaviorSubject<T>): Disposable =
            subscribe(single, { subject.onNext(it) }, { subject.onError(it) })

    fun <T> subscribe(tag: String, single: Single<T>): Disposable =
            helper.add(tag, single.subscribe())

    fun <T> subscribe(tag: String, single: Single<T>, next: (T) -> Unit): Disposable =
            helper.add(tag, single.subscribe(next))

    fun <T> subscribe(tag: String, single: Single<T>, next: (T) -> Unit, error: (Throwable) -> Unit): Disposable =
            helper.add(tag, single.subscribe(next, error))

    fun <T> subscribe(tag: String, single: Single<T>, observer: DisposableSingleObserver<T>): Disposable =
            helper.add(tag, single.subscribeWith(observer))
}

fun RxHelper.singleDelegate(): Lazy<SingleHelper> = lazy {
    SingleHelper(this, asyncScheduler, mainScheduler)
}