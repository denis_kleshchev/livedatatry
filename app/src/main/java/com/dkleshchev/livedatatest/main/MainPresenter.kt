package com.dkleshchev.livedatatest.main

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import com.dkleshchev.livedatatest.helper.RxHelper
import com.dkleshchev.livedatatest.mvp.BasePresenter
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class MainPresenter : BasePresenter<MainView>() {

    companion object {
        private val progressStateKey = "progress bar state key"
        private val progressPercentKey = "progress bar percent key"
    }

    private val rxHelper = RxHelper()
    private val viewState = getStateBundle()

    fun doStuff() {
        doIfAttached {
            it.showProgress()
            viewState.putBoolean(progressStateKey, true)
        }

        performStuff()
    }

    private fun doIfAttached(action: (MainView) -> Unit) {
        if (isViewAttached()) {
            getView()?.let(action)
        }
    }

    private fun performStuff() {
        rxHelper.add(
                rxHelper.observable.subscribe(
                        Observable.interval(1, TimeUnit.SECONDS)
                                .takeUntil { it >= 10 }
                                .compose(rxHelper.observable.applySchedulers()),
                        {
                            doIfAttached {
                                val percent = viewState.getInt(progressPercentKey) + 1
                                viewState.putInt(progressPercentKey, percent)
                                it.updateProgress(percent * 10)
                            }
                        },
                        {},
                        {
                            doIfAttached {
                                it.updateProgress(0)
                                it.hideProgress()
                                viewState.putInt(progressPercentKey, 0)
                                viewState.putBoolean(progressStateKey, false)
                            }
                        }
                )
        )
    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        doIfAttached {
            if (viewState.getBoolean(progressStateKey)) {
                it.showProgress()
                it.updateProgress(viewState.getInt(progressPercentKey) * 10)
            }
        }
    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        doIfAttached {
            it.hideProgress()
        }
    }

    override fun onPresenterDestroy() {
        super.onPresenterDestroy()
        rxHelper.clear()
    }
}