package com.dkleshchev.livedatatest.main

import android.app.ProgressDialog
import android.os.Bundle
import android.widget.Button
import com.dkleshchev.livedatatest.R
import com.dkleshchev.livedatatest.mvp.BaseActivity
import com.dkleshchev.livedatatest.mvp.LayoutId

@LayoutId(R.layout.activity_main)
class MainActivity : BaseActivity<MainView, MainPresenter>(), MainView {

    private val progressDialog by lazy {
        ProgressDialog(this).apply {
            setMessage("Loading")
            setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
            setCancelable(false)
            progress = 0
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        findViewById<Button>(R.id.button).setOnClickListener { presenter.doStuff() }
    }

    override fun initPresenter(): MainPresenter = MainPresenter()

    override fun showProgress() = progressDialog.show()

    override fun hideProgress() = progressDialog.hide()

    override fun updateProgress(current: Int) {
        progressDialog.progress = current
    }
}
