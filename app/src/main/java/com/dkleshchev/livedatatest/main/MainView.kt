package com.dkleshchev.livedatatest.main

import com.dkleshchev.livedatatest.mvp.BaseContract

interface MainView: BaseContract.View {
    fun showProgress()

    fun hideProgress()

    fun updateProgress(current: Int)
}